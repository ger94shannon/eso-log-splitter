#!/usr/bin/env python
import os
import re
from datetime import datetime


NEWEST_ZONE_ID = 1307


class ConsoleUI:

    @staticmethod
    def display_table_of_contents(table_of_contents):
        print('Found {} log sessions...'.format(table_of_contents.log_index + 1))
        for log in table_of_contents.logs:
            print('  {start_time} {zones} ({start}-{end} / {length})'.format(
                start_time=log.start_time.strftime('%Y-%m-%d %H:%M'),
                zones=log.zones,
                start=log.start_line,
                end=log.end_line,
                length=(log.end_line - log.start_line)
            ))

    @staticmethod
    def display_current_export(filename):
        print('  {}'.format(filename))

    @staticmethod
    def display_app_exception(exception):
        print('Error: {}'.format(exception.message))


class AppException(Exception):

    def __init__(self, message, exit_code=1):
        self.message = message
        self.exit_code = exit_code


class TableOfContents:

    def __init__(self):
        self.logs = []
        self.log_index = -1

    def add_character(self, line, line_number):
        self.logs[self.log_index].add_character(line, line_number)

    def begin_log(self, line, line_number):
        if self.log_index >= 0 and self.logs[self.log_index].end_line == 0:
            self.logs[self.log_index].end_line = line_number - 1
            self.logs[self.log_index].ended_unexpectedly = True
        self.logs.append(Log(line, line_number))
        self.log_index += 1

    def end_log(self, line, line_number):
        self.logs[self.log_index].end(line, line_number)

    def set_zone(self, line, line_number):
        self.logs[self.log_index].add_zone(line, line_number)


class Log:
    def __init__(self, line=None, line_number=None):
        self.zones = []
        self.start_line = 0
        self.start_time = 0
        self.end_line = 0
        self.ended_unexpectedly = False
        self.characters = {}

        if line and line_number:
            self.begin(line, line_number)

    def begin(self, line, number):
        r = re.search(r'(?P<uptime>\d+),BEGIN_LOG,(?P<timestamp>\d+),(\d+),'
                      r'"(?P<server>.*)","(?P<language>.*)","(?P<build>.*)"', line.strip())
        if not r:
            return
        self.start_line = number
        self.start_time = datetime.fromtimestamp(
            float(r.group('timestamp')) / 1000
        )

    def debug(self):
        print('Lines:      {} - {}'.format(self.start_line, self.end_line))
        print('Timestamp:  {}'.format(self.start_time))
        print('Zones:')
        for (zid, zone, difficulty) in self.zones:
            print('            {} {}'.format(difficulty.lower()[0], abbreviate_zone(zid, zone)))
        print('Characters:')
        for account, character in self.characters.items():
            print('            {} / {} (CP {})'.format(
                character.account,
                character.character,
                character.champion_points
            ))

    def end(self, line, number):
        self.end_line = number

    def add_character(self, line, number):
        character = Character(line)
        if character and character.account and character.account not in self.characters:
            self.characters[character.account] = character

    def add_zone(self, line, number):
        r = re.search(r'(?P<uptime>\d+),ZONE_CHANGED,(?P<zone_id>\d+),"(?P<zone>.*)",(?P<difficulty>.*)', line.strip())
        self.zones.append((int(r.group('zone_id')), r.group('zone'), r.group('difficulty')))


class Character:
    def __init__(self, line):
        self.account = None
        self.character = None
        self.champion_points = 0
        self.set(line)

    def set(self, line):
        r = re.search(r'"(?P<character>.*)","(?P<account>@.*)",(\d+),(\d+),(?P<cp>\d+)', line.strip())
        if not r:
            return
        self.account = r.group('account')
        self.character = r.group('character')
        self.champion_points = r.group('cp')


def abbreviate_zone(zid, zone):
    dictionary = {
        # Trials
        "Asylum Sanctorium": "AS",
        "Aetherian Archive": "AA",
        "Cloudrest": "CR",
        "Dreadsail Reef": "DSR",
        "Halls of Fabrication": "HOF",
        "Hel Ra Citadel": "HRC",
        "Kyne's Aegis": "KA",
        "Maw of Lorkhaj": "MOL",
        "Rockgrove": "RG",
        "Sanctum Ophidia": "SO",
        "Sunspire": "SS",
        # Arenas
        "Blackrose Prison": "BRP",
        "Dragonstar Arena": "DSA",
        "Maelstrom Arena": "MA",
        "Vateshran Hollows": "VH",
        # Dungeons - Base
        "Arx Corinium": "Arx",
        "Blackheart Haven": "BHH",
        "Blessed Crucible": "BC",
        "City of Ash I": "COA1",
        "City of Ash II": "COA2",
        "Crypt of Hearts I": "COH1",
        "Crypt of Hearts II": "COH2",
        "Darkshade Caverns I": "DC1",
        "Darkshade Caverns II": "DC2",
        "Direfrost Keep": "DK",
        "Elden Hollow I": "EH1",
        "Elden Hollow II": "EH2",
        "Fungal Grotto I": "FG1",
        "Fungal Grotto II": "FG2",
        "Selene's Web": "SW",
        "Spindleclutch I": "S1",
        "Spindleclutch II": "S2",
        "Tempest Island": "TI",
        "The Banished Cells I": "BC1",
        "The Banished Cells II": "BC2",
        "Vaults of Madness": "VOM",
        "Volenfell": "Vol",
        "Wayrest Sewers I": "WS1",
        "Wayrest Sewers II": "WS2",
        # Dungeons - DLC
        "Imperial City Prison": "ICP",
        "White-Gold Tower": "WGT",
        "Cradle of Shadows": "COS",
        "Ruins of Mazzatun": "Maz",
        "Bloodroot Forge": "BF",
        "Falkreath Hold": "FH",
        "Fang Lair": "FL",
        "Scalecaller Peak": "SP",
        "March of Sacrifices": "MOS",
        "Moon Hunter Keep": "MHK",
        "Depths of Malatar": "DOM",
        "Frostvault": "FV",
        "Lair of Maarselok": "LOM",
        "Moongrave Fane": "MGF",
        "Icereach": "IR",
        "Unhallowed Grave": "UG",
        "Castle Thorn": "CT",
        "Stone Garden": "SG",
        "Black Drake Villa": "BDV",
        "The Cauldron": "C",
        "Red Petal Bastion": "RPB",
        "The Dread Cellar": "TDC",
        "Coral Aerie": "CA",
        "Shipwright's Regret": "SR",
        # PvP
        "Cyrodiil": "Cyro",
        "Ald Carac": "AC",
        "Arcane University": "AU",
        "Ularra": "Ul",
        "Foyada Quarry": "FQ",
        "Mor Khazgur": "MG",
        "Deeping Drome": "DD",
        "Istirus Outpost": "IO",
        "Eld Angavar": "EA",
    }
    if zone in dictionary:
        return dictionary[zone]
    elif zid < NEWEST_ZONE_ID:
        return None
    else:
        return zone.replace(' ', '_')


def scan_file(filename, table_of_contents):

    try:
        with open(filename, encoding="utf8") as f:
            line_number = 0
            for line in f:
                line_number += 1
                if "BEGIN_LOG" in line:
                    table_of_contents.begin_log(line, line_number)
                elif "END_LOG" in line:
                    table_of_contents.end_log(line, line_number)
                elif "UNIT_ADDED" in line and "PLAYER_ALLY" in line:
                    table_of_contents.add_character(line, line_number)
                elif "ZONE_CHANGED" in line:
                    table_of_contents.set_zone(line, line_number)
    except FileNotFoundError:
        raise AppException(message='There is no Encounter.log to split in this directory')


def get_zone_list_string(zones):
    zone_list = []
    for (zid, zone, difficulty) in zones:
        abbr = abbreviate_zone(zid, zone)
        if abbr:
            zone_list.append('{}{}'.format(difficulty.lower()[0], abbr))
    return '+'.join(zone_list)


def record_corruption(source_object, last_successful_log, corrupt_line):
    print('Unexpected error on line {} ({} of corrupt output)'.format(
        corrupt_line, corrupt_line-last_successful_log.end_line
    ))
    with open('Encounter.corrupt.log', 'w', encoding="utf8") as c:
        c.write(source_object[last_successful_log.end_line+1:])


def split_file(filename, table_of_contents):
    current_log_index = 0
    log = table_of_contents.logs[current_log_index]
    with open(filename, encoding="utf8") as s:
        line_number = 0
        try:
            for line in s:
                line_number += 1
                if line_number == log.start_line:
                    new_filename = 'Encounter.{date}.{zones}.log'.format(
                        date=log.start_time.strftime('%Y%m%d.%H%M'),
                        zones=get_zone_list_string(log.zones)
                    )
                    ConsoleUI.display_current_export(new_filename)
                    o = open(new_filename, 'w', encoding="utf8")
                if log.start_line <= line_number <= log.end_line:
                    o.write(line)
                if line_number == log.end_line:
                    o.close()
                    current_log_index += 1
                    log = table_of_contents.logs[current_log_index] if current_log_index <= table_of_contents.log_index else None
        except Exception as e:
            record_corruption(
                source_object=s,
                last_successful_log=table_of_contents.logs[current_log_index-1],
                corrupt_line=line_number+1
            )
            raise e
    os.replace(filename, filename.replace('.log', '.split.log'))
    print('Done')


def debug(bookmarks):
    for encounter in bookmarks:
        encounter.debug()


def run():
    try:
        encounter_log_name = 'Encounter.log'
        table_of_contents = TableOfContents()
        scan_file(encounter_log_name, table_of_contents)
        # ConsoleUI.display_table_of_contents(table_of_contents)
        split_file(encounter_log_name, table_of_contents)
    except AppException as e:
        ConsoleUI.display_app_exception(e)
        exit(e.exit_code)


run()
