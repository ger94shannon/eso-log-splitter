# ESO Log Splitter

The purpose of this script is to split ESO encounter log files which include multiple sessions
into individual session files.  For now, this means every time `/encounterlog` was started again,
a new file will be exported.

## Requirements

This script was written for Python 3.8 and has not been tested for any other version.

## Usage 

- Run this script from the same directory as your Encounter.log file.
- New log files will be created for each session titled with the start time and zones.
- Colliding files will be overwritten but should always be overwritten with the same contents as before.

## Future Plans

- Allow specifying a different source file.
- Add abbreviations for all trials and arenas.
- Potentially add abbreviations for all dungeons.
- Ignore all overland zones when naming files.
- Look into splitting files within the same session on zone changes.  (Might not be possible.)
- Selectively choose which portions to split and which to keep together.
- Option to merge separate files back together.